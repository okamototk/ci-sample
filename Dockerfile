FROM golang:latest as codescan
WORKDIR /go/src/gitlab.com/cloudnativetips/ci-sample
COPY . .
RUN go vet $(go list ./... | grep -v /vendor/)

FROM golang:latest as unittest
WORKDIR /go/src/gitlab.com/cloudnativetips/ci-sample
COPY . .
RUN go install github.com/jstemmer/go-junit-report@v1.0.0 && \
go test -race -v $(go list ./... | grep -v /vendor/) 2>&1 | \
go-junit-report -set-exit-code > report.xml && \
go test -race $(go list ./... | grep -v /vendor/) -cover > cover.txt

FROM golang:latest as build_app
WORKDIR /go/src/gitlab.com/cloudnativetips/ci-sample
COPY . .
ARG CGO_ENABLED=0
RUN go build -o app cmd/main.go

FROM scratch as build_image
COPY --from=build_app /go/src/gitlab.com/cloudnativetips/ci-sample/app /app
ENTRYPOINT ["/app"]
